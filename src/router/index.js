import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/views/Index'
import PostID from '@/views/PostID'
import CreatePost from '@/views/CreatePost'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/post/:id',
      props: true,
      name: 'PostID',
      component: PostID
    },
    {
      path: '/create-post',
      name: 'CreatePost',
      component: CreatePost
    }
  ]
})
